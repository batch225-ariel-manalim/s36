// index.js => route => controller => models

// Setup the dependencies
const express = require('express');
const mongoose = require("mongoose");
// npm install dotenv
const dotenv = require("dotenv").config(); 
const taskRoute = require("./routes/taskRoute");


// Server Setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database Connection
// Connect to Mongo atlas

mongoose.connect(`mongodb+srv://ayei20:${process.env.PASSWORD}@cluster0.dxlfk3e.mongodb.net/MCR?retryWrites=true&w=majority`,
{
	useNewUrlParser: true,
	useUnifiedTopology: true
}
);

// Setup notification for connection sucess or failure
let db = mongoose.connection;
db.on("err", console.error.bind(console, "connection error"));
db.on('open', () => console.log('Connected to MongoDB'));


// Add the task routes
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route

app.use("/tasks", taskRoute);

// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));
